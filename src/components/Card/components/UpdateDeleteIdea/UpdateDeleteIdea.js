import React, { useState } from "react"
import axios from "axios"

import "./UpdateDeleteIdea.scss"
import Card from "../../Card"
import Button from "../../../Button/Button"

const ReadDeleteIdea = props => {
	const { id, idea, setState } = props
	const [ideaUpdate, setIdeaUpdate] = useState(idea)

	const deleteIdea = () => {
		axios.delete(`/api/${id}`).then(({ status }) => {
			if (status === 200)
				axios.get("/api").then(({ data: ideas }) => {
					setState(ideas)
				})
		})
	}

	const handleIdeaUpdate = e => {
		setIdeaUpdate(e.target.value)
	}

	const updateIdea = () => {
		axios.post("/api", {
			id,
			idea: ideaUpdate
		})
	}

	return (
		<Card>
			<h2>Idea</h2>
			<textarea cols="5" value={ideaUpdate} onChange={handleIdeaUpdate}>
				{props.idea}
			</textarea>
			<div className="idea-options">
				<Button text="Delete" type="outline" size="sm" onClick={deleteIdea} />
				<Button text="Update" type="secondary" size="sm" onClick={updateIdea} />
			</div>
		</Card>
	)
}

export default ReadDeleteIdea
