import React, { useState } from "react"
import axios from "axios"

import "./CreateIdea.scss"
import Card from "../../Card"
import Button from "../../../Button/Button"

const CreateIdea = props => {
	const [idea, setIdea] = useState("")

	const handleChange = e => {
		setIdea(e.target.value)
	}

	const createIdea = () => {
		axios
			.put("/api", {
				idea
			})
			.then(({ status }) => {
				if (status === 200)
					axios.get("/api").then(({ data: ideas }) => {
						props.setState(ideas)
					})
				else console.log("An error occured while trying to create your idea")
			})
	}

	return (
		<Card>
			<h2>Idea</h2>
			<textarea cols="5" onChange={handleChange} value={idea} />
			<Button text="Create" type="primary" onClick={createIdea} />
		</Card>
	)
}

export default CreateIdea
